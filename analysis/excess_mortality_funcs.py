from pathlib import Path
import numpy as np
import pandas as pd
import xarray as xr

import pytest
import matplotlib.pyplot as plt
import matplotlib
from pprint import pprint

BEGIN = pd.to_datetime('2000-01-01')

def parse_eurostat(path, is_calc_0_to_64=False):
    df_stat = pd.read_csv(path, usecols=['TIME', 'GEO', 'AGE', 'Value'], index_col=['TIME', 'GEO', 'AGE'], na_values=[':'], thousands=',').Value.dropna().astype(int).unstack(['GEO', 'AGE'])
    columns = df_stat.columns.to_frame(False)
    columns.AGE = columns.AGE.str.replace('From ', '').str.replace(' to ', '-').str.replace(' years' ,'').str.replace(' or over', '-120')
    df_stat.columns = pd.MultiIndex.from_frame(columns)
    df_stat.index = pd.to_datetime(df_stat.index.str.replace('W', '-') + '-7', format='%G-%V-%u')
    if is_calc_0_to_64:
        a = df_stat.stack().stack().to_xarray()
        df_stat = (xr.concat([(a.sel(AGE='Total') - a.sel(AGE=slice('65-69', '90-120')).sum(dim='AGE')).expand_dims('AGE').assign_coords(AGE=['0-64']), a], dim='AGE', )).to_series().unstack('GEO').unstack('AGE')
    return df_stat


def parse_cdc_1(path_usa):
    out2 = pd.read_csv(path_usa, skiprows=4, parse_dates=['Week Ending Date'])
    out2.Type = out2.Type.map({'Predicted (weighted)' : True, 'Unweighted' : False})
    out2.rename(columns={'Type' : 'is_predicted', 'Age Group' : 'AGE', 'Jurisdiction' : 'GEO', 'Week Ending Date' : 'TIME'}, inplace=True)
    out2.AGE = out2.AGE.str.replace('Under ', '0-').str.replace(' years', '').str.replace(' and older', '-120')
    out2 = out2.loc[:, ['GEO', 'TIME', 'AGE', 'is_predicted', 'Number of Deaths']].set_index(['GEO', 'TIME', 'AGE', 'is_predicted']).squeeze()
    #out2_all_states = out2.loc['United States']
    #out2_all_states = out2_all_states.xs(True, level='is_predicted')
    #out2_all_states = out2_all_states.unstack('AGE')
    out2 = out2.xs(True, level='is_predicted')
    out2 = out2.unstack('GEO').unstack('AGE')
    return out2

def day_to_date(day, BEGIN=BEGIN):

    date = pd.TimedeltaIndex(day, 'days') + BEGIN
    return date

def date_to_day(date, BEGIN=BEGIN):
    days = (date - BEGIN).days
    return days

   
#test_day_to_date_and_back()

def date_index_weekly_resolution_to_day_index_daily_resolution(series_population, BEGIN=BEGIN):
    dates = series_population.index
    is_consecutive_weeks = (np.diff((dates - BEGIN).days.values) == 7).all()
    assert is_consecutive_weeks, NotImplementedError('weeks in data are not consecutive')
    days = pd.RangeIndex(start=(series_population.index[0] - BEGIN).days - 6, stop=(series_population.index[-1] - BEGIN).days + 1)
    out = pd.Series(index=days, data=np.repeat(series_population.values[:, np.newaxis] / 7, 7))
    return out


#.plot()
#test_date_index_weekly_resolution_to_day_index_daily_resolution()



def weekly_resolution_to_daily_resolution(series_population):
    dates = series_population.index
    BEGIN = dates[0]
    is_consecutive_weeks = (np.diff((dates - BEGIN).days.values) == 7).all()
    assert is_consecutive_weeks, NotImplementedError('weeks in data are not consecutive')
    days = pd.RangeIndex(start=(series_population.index[0] - BEGIN).days - 6, stop=(series_population.index[-1] - BEGIN).days + 1)
    out = pd.Series(index=day_to_date(days, BEGIN), data=np.repeat(series_population.values[:, np.newaxis] / 7, 7))
    return out


def rolling_short_term_linear_prediction(series, N=4 * 365, N_delay=180):
    #b = pd.DataFrame(index=series.index, data={'x' : date_to_day(series.index).values, 'y' : series})
    b = pd.DataFrame(index=series.index, data={'x' : series.index.values, 'y' : series})
    c = b.rolling(N).cov().unstack()
    slopes = c.loc[:, ('x', 'y')] / c.loc[:, ('x', 'x')]
    biases = b.y.rolling(N).mean() - slopes * b.x.rolling(N).mean()
    trend = biases.shift(N_delay) + slopes.shift(N_delay) * b.x
    return trend


def sanitize_mortality_data(s):
    first_nonzero_idx = (s > 0.5).values.nonzero()[0][0]
    return s.iloc[first_nonzero_idx:] #for "stupid zeros"


def get_excess_derivation(mortality_daily):
    excess = mortality_daily
    #duration_years = int(np.ceil((mortality_daily.index[-1] - mortality_daily.index[0]) / 365))
    duration_years = int(np.ceil((mortality_daily.index[-1] - mortality_daily.index[0]).days / 365))
    #fitting_slice = slice(None, date_to_day(pd.Timestamp('2019-12-31')))
    fitting_slice = slice(None, pd.Timestamp('2019-12-31'))

    mortality_daily_fit = mortality_daily.loc[fitting_slice]
    #groupby_day_sun_phase = mortality_daily_fit.groupby(lambda day : (day  - mortality_daily.index[0]) % 365)
    groupby_day_sun_phase = mortality_daily_fit.reset_index(drop=True).groupby(lambda day : day % 365)

    seasonal_component = groupby_day_sun_phase.mean()
    #corresponding_days_std = groupby_sunphase.std()
    seasonal_component = pd.Series(index=mortality_daily.index, data=np.concatenate(duration_years * [seasonal_component.values,])[:len(mortality_daily)])
    #corresponding_days_std = pd.Series(index=mortality_daily.index, data=np.concatenate(duration_years * [corresponding_days_std.values,])[:len(out)])
    excess = excess - seasonal_component
    
    
    if True:
        n_years_for_trend = int(np.floor((len(excess) - 180 - 365 - 1) / 365))
        n_years_for_trend = min(5, n_years_for_trend)
        linear_trend_component = rolling_short_term_linear_prediction(pd.Series(excess.values, date_to_day(excess.index)), N=n_years_for_trend*365, N_delay=180)
        linear_trend_component.index = excess.index

    else:
        fitter = linear_model.LinearRegression()
        #fitter.fit(excess.loc[fitting_slice].index.values.reshape(-1, 1), excess.loc[fitting_slice].values.reshape(-1, 1))
        fitter.fit(np.arange(len(excess.loc[fitting_slice])).reshape(-1, 1), excess.loc[fitting_slice].values.reshape(-1, 1))
        #print(fitter.coef_, fitter.intercept_)
        #xy.plot()
        #linear_trend_component = pd.Series(index=excess.index, data=fitter.predict(excess.index.values.reshape(-1, 1)).reshape(-1))
        linear_trend_component = pd.Series(index=excess.index, data=fitter.predict(np.arange(len(excess)).reshape(-1, 1)).reshape(-1))
    
    excess = excess - linear_trend_component
    
    cumulative_excess_one_year = excess.rolling(365).sum()

    excess_derivation = pd.concat([mortality_daily, seasonal_component, linear_trend_component, excess, cumulative_excess_one_year], keys=['mortality', 'seasonal_component', 'linear_trend_component', 'excess', 'cumulative_excess_one_year'], axis=1)
    return excess_derivation


def excess_derivation_to_weekly_resolution(excess_derivation):
    #excess_derivation = excess_derivation.copy()
    #excess_derivation.index = BEGIN + pd.TimedeltaIndex(excess_derivation.index, 'days')
    
    excess_derivation_weekly = pd.concat([
        excess_derivation.mortality.rolling(7).sum().iloc[6::7],
        excess_derivation.seasonal_component.rolling(7).sum().iloc[6::7],
        excess_derivation.linear_trend_component.rolling(7).sum().iloc[6::7],
        excess_derivation.excess.rolling(7).sum().iloc[6::7],
        excess_derivation.cumulative_excess_one_year.iloc[6::7],
        ],
        keys=excess_derivation.columns,
        axis=1
    )
    #excess_derivation_weekly.index = BEGIN + pd.TimedeltaIndex(excess_derivation.index, 'days')[6::7]
    excess_derivation_weekly.index = excess_derivation.index[6::7]

    return excess_derivation_weekly


def calculate_mortality_values(excess_derivation, reference_date=None, last_fit_date='2019-12-31'):
    if type(last_fit_date) is str:
        last_fit_date = pd.Timestamp(last_fit_date) 
    
    if reference_date is None:
        print(last_fit_date + pd.Timedelta(1, 'days'))
        reference_date = excess_derivation.loc[last_fit_date + pd.Timedelta(1, 'days'):, 'cumulative_excess_one_year'].idxmax()
        #print(excess_derivation.excess)
        #print(reference_date)
    #reference_date = pd.Timestamp('2020-07-01')
    annual_mortality_each_date = excess_derivation.mortality.rolling(365).sum()

    annual_mortality_at_reference = annual_mortality_each_date.loc[reference_date]
    model_annual_mortality_at_reference = excess_derivation.seasonal_component.loc[:reference_date].iloc[-365:].sum() + excess_derivation.linear_trend_component.loc[:reference_date].iloc[-365:].sum()

    yearly_mortality_mean = annual_mortality_each_date.loc[:last_fit_date].mean()
    yearly_mortality_std = annual_mortality_each_date.loc[:last_fit_date].std()
    n_sigma = (annual_mortality_at_reference - yearly_mortality_mean) / yearly_mortality_std

    out = {'last_fit_date' : last_fit_date,
            'reference_date' : reference_date,
           'annual_mortality_at_reference' : annual_mortality_at_reference,
           'model_annual_mortality_at_reference' : model_annual_mortality_at_reference,
           'excess_annual_mortality_at_reference' : annual_mortality_at_reference - model_annual_mortality_at_reference,
           'relative_annual_mortality_at_reference' : (annual_mortality_at_reference - model_annual_mortality_at_reference) / model_annual_mortality_at_reference,
           'yearly_mortality_mean' : yearly_mortality_mean,
           'yearly_mortality_std' : yearly_mortality_std,
           'n_sigma' : n_sigma,
           
    }
    
    
    return out



def plot_excess_derivation_weekly_results(excess_derivation, excess_derivation_weekly=None, axes=None, path_save=None):
    #excess_derivation = excess_derivation.copy()
    #excess_derivation.index = BEGIN + pd.TimedeltaIndex(excess_derivation.index, 'days')

    if excess_derivation_weekly is None:
        excess_derivation_weekly = excess_derivation_to_weekly_resolution(excess_derivation)

    if axes is None:
        # note: the next few lines are duplicated from read_compute_plot
        fig, axes = plt.subplots(2, 2, sharex=True, figsize=(18, 18))
        #title = f'Population: {country}, Age: {age_rep}, Adjustment: {adjustment}'
        title = 'title'
        fig.suptitle(title)
    else:
        fig = axes[0][0].get_figure()
        title = fig._suptitle
        if title is None:
            title = 'title'
        else:
            title = title.get_text()



    if False:
        cumulative_excess_at_2020 = excess_derivation_weekly.cumulative_excess_one_year.loc['2020-01-01':]
        max_excess_2020_date = cumulative_excess_at_2020.index[cumulative_excess_at_2020.argmax()]
        excess_max_2020 = cumulative_excess_at_2020.max()
        #typical_yearly_mortality = excess_derivation.seasonal_component.iloc[:365].sum() + excess_derivation.linear_trend_component.mean() * 365
        typical_yearly_mortality = excess_derivation.seasonal_component.iloc[-365:].sum() + excess_derivation.linear_trend_component.iloc[-1] * 365

        yearly_mortality_mean = excess_derivation.mortality.rolling(365).sum().loc[:'2019-12-31'].mean()
        yearly_mortality_std = excess_derivation.mortality.rolling(365).sum().loc[:'2019-12-31'].std()
        reference_date = pd.Timestamp('2020-07-01')
        n_sigma = (excess_derivation.mortality.rolling(365).sum().loc[reference_date] - yearly_mortality_mean) / yearly_mortality_std

    elif True:
        mortality_values = calculate_mortality_values(excess_derivation)
        reference_date, n_sigma, typical_yearly_mortality, max_excess_2020_date, excess_max_2020 = \
        [mortality_values[k] for k in ['reference_date', 'n_sigma', 'model_annual_mortality_at_reference', 'reference_date', 'excess_annual_mortality_at_reference']]
        #pprint(mortality_values)
        #print(reference_date)

    excess_derivation_weekly.mortality.plot(ax=axes[0,0], style='.', title=f'Weekly Mortality Data')
    (excess_derivation_weekly.seasonal_component + excess_derivation_weekly.linear_trend_component).plot(ax=axes[0, 0], style='--', label='seasonal + trend')
    axes[0,0].legend()
    axes[0,0].grid(which='major', axis='y')


    excess_derivation.mortality.rolling(365).sum().plot(ax=axes[0, 1], title=f'Total mortality in year preceding each date \nAt {reference_date.date()} equivalent to {n_sigma:.2f}$\sigma$', label='')
    (excess_derivation.seasonal_component + excess_derivation.linear_trend_component).rolling(365).sum().plot(ax=axes[0, 1], label='Trend')
    axes[0, 1].axhline(y=mortality_values['annual_mortality_at_reference'], linestyle='--', color='green', label=f'{mortality_values["annual_mortality_at_reference"]:f}')
    axes[0, 1].axvline(max_excess_2020_date, label=f'Excess peak at {max_excess_2020_date.date()}', linestyle='--', color='black')
    axes[0, 1].legend()


    excess_derivation_weekly.excess.plot(ax=axes[1,0], style='.', title='Excess Weekly Mortality')
    axes[1,0].axhline(y=0, linestyle='-', color='black', label='zero')
    axes[1,0].legend()
    axes[1,0].grid(which='major', axis='y')



    excess_derivation_weekly.cumulative_excess_one_year.plot(ax=axes[1,1], style='.', title=f'Excess Mortality in year preceding each date')

    axes[1, 1].set_ylabel('Nominal')
    ax_excess_sum_normalized = plt.twinx(axes[1,1])
    ax_excess_sum_normalized.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())
    ax_excess_sum_normalized.set_ylabel(f'relative to average {"year"}')
    ax_excess_sum_normalized.set_ylim(np.array(axes[1,1].get_ylim()) / typical_yearly_mortality * 100)



    axes[1,1].axvline(max_excess_2020_date, label=f'Excess peak at {max_excess_2020_date.date()}', linestyle='--', color='black')

    axes[1,1].set_title(axes[1,1].get_title() + f'\nYearly nominal death: {typical_yearly_mortality:.0f} Max Excess Nominal: {excess_max_2020:.0f} relative: {excess_max_2020 / typical_yearly_mortality * 100:2.1f}%')

    axes[1,1].legend(loc='upper left')
    axes[1,1].grid(which='major', axis='y')

    [ax.set_xlim(ax.get_xlim()[0], ax.get_xlim()[1] + 10) for ax in axes.flatten()]

    
    if path_save:
        path = path_save.joinpath(title + '.pdf')
        fig.savefig(path)
    else:
        path = None

    return fig, axes, path


def read_compute_plot(country, age, data=None, series_population=None, population=None, path_save=None, age_rep=None):
    if age_rep is None:
        age_rep = age 
    if population is None:
        adjustment = None
    else:
        adjustment = 'population of 2020'
    
    if series_population is None:
        series_population = data.loc[:, (country, age)].dropna().astype(int)
    series_population = sanitize_mortality_data(series_population)
    
    mortality_daily = weekly_resolution_to_daily_resolution(series_population)
    if not population is None:
        mortality_daily = mortality_daily / population.loc[:, (country, age)] * population.loc[:, (country, age)].iloc[-1]
    excess_derivation = get_excess_derivation(mortality_daily)
    #excess_derivation.plot(subplots=True)
    mortality_values = calculate_mortality_values(excess_derivation)

    excess_derivation_weekly = excess_derivation_to_weekly_resolution(excess_derivation)
    #fig, axes = plt.subplots(3, sharex=True, figsize=(12, 12))
    pprint(mortality_values)
    fig, axes = plt.subplots(2, 2, sharex=True, figsize=(18, 18))
    title = f'Population: {country}, Age: {age_rep}, Adjustment: {adjustment}'
    fig.suptitle(title)
    plot_excess_derivation_weekly_results(excess_derivation, excess_derivation_weekly, axes)

    if path_save:
        path = path_save.joinpath(title + '.pdf')
        fig.savefig(path)
    else:
        path = None
        
    return excess_derivation, fig, axes, path



def calculate_mortality_values_for_ages_cumulative_ages_countries(df_stat, grid, population=None):
    covid_excess = {}
    for country, age in grid:
        print(country, age)
        # interface country age data population
        age_rep = age
        try:
            series_population = df_stat.loc[:, (country, age)].dropna()
            series_population = sanitize_mortality_data(series_population)
            mortality_daily = weekly_resolution_to_daily_resolution(series_population)
            if not population is None:
                mortality_daily = mortality_daily / population.loc[:, (country, age)] * population.loc[:, (country, age)].iloc[-1]
            excess_derivation = get_excess_derivation(mortality_daily)
            mortality_values = calculate_mortality_values(excess_derivation)

            covid_excess[(country, age)] = mortality_values
        except Exception as e:
            print(e)

    covid_excess = pd.DataFrame(data=covid_excess, ).T
    covid_excess.index.names = ['country', 'age']

    covid_excess_fine_age = covid_excess.loc[:, ['annual_mortality_at_reference', 'model_annual_mortality_at_reference', 'excess_annual_mortality_at_reference']]
    #a.drop('Total').sum(axis=0) - a.loc['Total']
    covid_excess_0_to_age = covid_excess_fine_age.drop('Total', level='age', errors='ignore').unstack('country').cumsum(axis=0).stack('country').reorder_levels(['country', 'age']).sort_index(level='country').rename(index = lambda s : f"0-{s.split('-')[1]}", level='age')
    #c = (b.iloc[-1] - b).drop('0-120').rename(index = lambda s : f"{int(s.split('-')[1]) + 1}-120")
    covid_excess_age_to_120 = (covid_excess_0_to_age.xs('0-120', level='age') - covid_excess_0_to_age.drop('0-120', level='age')).rename(index = lambda s : f"{int(s.split('-')[1]) + 1}-120", level='age')

    for covid_excess_ in [covid_excess_fine_age, covid_excess_0_to_age, covid_excess_age_to_120]:
        covid_excess_['excess_relative'] = covid_excess_.excess_annual_mortality_at_reference / covid_excess_.model_annual_mortality_at_reference
        covid_excess_['excess_relative_percent'] = (covid_excess_.excess_relative * 100).astype(float).round(2).astype(str) + '%'

    return covid_excess_fine_age, covid_excess_0_to_age, covid_excess_age_to_120


def save_plot_covid_excess(covid_excess_fine_age, covid_excess_0_to_age, covid_excess_age_to_120, adjustment=None, path_results=None):
    if not path_results is None:
        covid_excess_fine_age.to_csv(path_results.joinpath(f'fine_age_adjustment_{adjustment}.csv'))
        covid_excess_0_to_age.to_csv(path_results.joinpath(f'zero_to_age_adjustment_{adjustment}.csv'))
        covid_excess_age_to_120.to_csv(path_results.joinpath(f'age_to_120_adjustment_{adjustment}.csv'))

    fig_fine, ax = plt.subplots()
    (covid_excess_fine_age.excess_relative * 100).unstack('age').plot.bar(ax=ax, rot=45, title='relative excess annual mortality eurostat')
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())
    if not path_results is None:
        fig_fine.savefig(path_results.joinpath(f'fine_age_adjustment_{adjustment}.jpeg'))

    fig_to_age, ax = plt.subplots()
    (covid_excess_0_to_age.excess_relative * 100).unstack('age').plot.bar(ax=ax, rot=45, title='relative excess annual mortality eurostat')
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())
    if not path_results is None:
        fig_to_age.savefig(path_results.joinpath(f'zero_to_age_adjustment_{adjustment}.jpeg'))

    fig_from_age, ax = plt.subplots()
    (covid_excess_age_to_120.excess_relative * 100).unstack('age').plot.bar(ax=ax, rot=45, title='relative excess annual mortality eurostat')
    ax.yaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())
    if not path_results is None:
        fig_from_age.savefig(path_results.joinpath(f'age_to_120_adjustment_{adjustment}.jpeg'))

    return fig_fine, fig_to_age, fig_from_age



def plot_compare_years_flu_weeks(excess_derivation, axes):
    a = excess_derivation.mortality
    b = date_to_day(a.index) - date_to_day(pd.Timestamp('2017-08-30'))
    a.index = pd.MultiIndex.from_frame(pd.DataFrame({'year' : b // 365 + 2018, 'day' : b %365}))

    #comparison_years = (2020, 2019)

    for comparison_years, ax in zip([(2020, 2019), (2020, 2018), (2019, 2018)], axes[1:]):
        c = (a.xs(comparison_years[0], level='year') - a.xs(comparison_years[1], level='year')).cumsum()
        c.index = pd.date_range(f'{comparison_years[0] - 1}-08-30', freq='D', periods=len(c))
        c.plot(ax=ax, title=f'compare year {comparison_years[0]}-{comparison_years[1]}')

    excess_derivation.excess.loc['2019-08-30':].cumsum().plot(ax=axes[0], title='compare year 2020 with model')
    axes[0].set_xlim(*axes[1].get_xlim())

    return axes

def parse_population(path):
    #path = path_data_4.joinpath('population.csv')
    a = pd.read_csv(path, usecols=['TIME', 'GEO', 'AGE', 'Value'], index_col=['TIME', 'GEO', 'AGE'], na_values=[':'], thousands=',').Value.dropna().astype(int).unstack(['GEO', 'AGE'])
    columns = a.columns.to_frame(False)
    columns.AGE = columns.AGE.str.replace('From ', '').str.replace(' to ', '-').str.replace(' years' ,'').str.replace(' or over', '-120')
    a.columns = pd.MultiIndex.from_frame(columns)
    b = a.stack('AGE').drop('75-120', level='AGE').stack().to_xarray()

    population = (xr.concat([(b.sel(AGE='Total') - b.sel(AGE=slice('65-69', '80-120')).sum(dim='AGE')).expand_dims('AGE').assign_coords(AGE=['0-64']),
        b.sel(AGE=['65-69', '70-74', '75-79', '80-84']),
     (b.sel(AGE='80-120') - b.sel(AGE='80-84')).expand_dims('AGE').assign_coords(AGE=['85-120'])
    ], dim='AGE')).to_series().unstack('GEO').unstack('AGE')
    return population

def population_extrapolation_intrapolation(population):
    yearly_growth = np.ceil(population.loc[2016:2019].diff().mean(axis=0)).astype(int)
    population = pd.concat([population.T, pd.DataFrame({2020 : population.loc[2019] + yearly_growth, 2021 : population.loc[2019] + 2 * yearly_growth})], axis=1).T
    population.index = pd.DatetimeIndex(population.index.astype(str) + '-01-01')
    population = population.resample('D').mean().interpolate()
    return population

def align_df_stat_to_population(df_stat):
    df_stat_2 = df_stat.stack().stack().to_xarray()
    df_stat_2 = xr.concat([df_stat_2.sel(AGE=slice('0-64', '80-84')), df_stat_2.sel(AGE=['85-89', '90-120']).sum(dim='AGE').expand_dims('AGE').assign_coords(AGE=['85-120'])], dim='AGE')
    df_stat_2 = df_stat_2.to_series().unstack('GEO').unstack('AGE')
    return df_stat_2
