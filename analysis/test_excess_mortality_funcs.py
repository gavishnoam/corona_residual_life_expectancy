from pathlib import Path
import itertools
import numpy as np
import pandas as pd
import xarray as xr

import pytest
import matplotlib.pyplot as plt

from excess_mortality_funcs import BEGIN, parse_eurostat, day_to_date, date_to_day, date_index_weekly_resolution_to_day_index_daily_resolution, weekly_resolution_to_daily_resolution
from excess_mortality_funcs import get_excess_derivation, rolling_short_term_linear_prediction, calculate_mortality_values
from excess_mortality_funcs import excess_derivation_to_weekly_resolution, plot_excess_derivation_weekly_results, read_compute_plot
from excess_mortality_funcs import calculate_mortality_values_for_ages_cumulative_ages_countries
from excess_mortality_funcs import parse_population, population_extrapolation_intrapolation, align_df_stat_to_population
from excess_mortality_funcs import sanitize_mortality_data
from excess_mortality_funcs import parse_cdc_1


def test_parse_eurostat():
    path = Path('../../mortality/eurostat/extract_1.csv')
    df_stat = parse_eurostat(path, is_calc_0_to_64=True)
    


def test_parse_csc_1():
    path_usa = Path('../../mortality/USA/Weekly_counts_of_deaths_by_jurisdiction_and_age_group.csv')
    df = parse_cdc_1(path_usa)

def test_parse_population_and_population_intrapolaltion_extrapolation():
    path = Path('../../mortality/eurostat/population.csv')
    population = parse_population(path)
    population = population_extrapolation_intrapolation(population)



@pytest.fixture
def df_stat():
    path = Path('../../mortality/eurostat/extract_1.csv')
    df_stat = parse_eurostat(path, is_calc_0_to_64=True)
    return df_stat 


@pytest.fixture
def day():
    return np.arange(10)

@pytest.fixture
def country():
    return 'Italy'


@pytest.fixture
def age():
    return 'Total'

def test_day_to_date_and_back(day, BEGIN=BEGIN):
    assert all(date_to_day(day_to_date(day)) == day)




def test_date_index_weekly_resolution_to_day_index_daily_resolution():
    series_population = pd.Series(index=day_to_date(np.arange(100) * 7), data=np.random.randint(100))
    out = date_index_weekly_resolution_to_day_index_daily_resolution(series_population)
#out.groupby(lambda day : int(day/7)).sum().plot()
    assert np.allclose(out.values.reshape(-1, 7).sum(axis=1), series_population)#.rolling(7).sum().iloc[::7]
    assert np.allclose(out.rolling(7).sum()[6::7], series_population)




def test_weekly_resolution_to_daily_resolution():
    s = pd.Series(index=pd.DatetimeIndex(['2000-01-10', '2000-01-17']), data=[7, 14]).astype(float)
    expected = pd.Series(index=pd.date_range('2000-01-04', '2000-01-17', freq='D'), data=7*[1, ] + 7 * [2, ]).astype(float)
    assert weekly_resolution_to_daily_resolution(s).equals(expected)

#weekly_resolution_to_daily_resolution(series_population).plot()
#(series_population / 7).plot()

#test_weekly_resolution_to_daily_resolution()

def test_visual_weekly_resolution_to_daily_resolution(df_stat):
    country = 'Italy'
    series_population = df_stat.loc[:, (country, 'Total')].dropna().astype(int)
    #series_population.index = pd.to_datetime(series_population.index.str.replace('W', '-') + '-7', format='%G-%V-%u')
    series_population.plot(style='.')

    fig, ax = plt.subplots()
    mortality_daily = weekly_resolution_to_daily_resolution(series_population)
    mortality_daily.plot()

#test_visual_weekly_resolution_to_daily_resolution(df_stat)


def test_rolling_short_term_linear_prediction():
    s = pd.Series(np.arange(10)).astype(float)
    out = rolling_short_term_linear_prediction(s, N=9, N_delay=1)
    expected = pd.Series(index=[9,], data=[9,])
    assert all((out - expected).dropna() == 0)

@pytest.mark.parametrize('country', ['Italy', 'Sweden'])
@pytest.mark.parametrize('age', ['Total', '65-69'])
def test_get_excess_derivation(country, age, df_stat):
    data = df_stat
    series_population = data.loc[:, (country, age)].dropna().astype(int)
    mortality_daily = weekly_resolution_to_daily_resolution(series_population)
    excess_derivation = get_excess_derivation(mortality_daily)


@pytest.fixture
def excess_derivation(country, age, df_stat):
    data = df_stat
    series_population = data.loc[:, (country, age)].dropna().astype(int)
    mortality_daily = weekly_resolution_to_daily_resolution(series_population)
    excess_derivation = get_excess_derivation(mortality_daily)
    return excess_derivation


def test_excess_derivation_to_weekly_resolution(excess_derivation):
    excess_derivation_to_weekly_resolution(excess_derivation)



def test_calculate_mortality_values(excess_derivation):
    #country = 'Italy'
    #series_population = df_stat.loc[:, (country, 'Total')].dropna().astype(int)
    #mortality_daily = weekly_resolution_to_daily_resolution(series_population)
    #excess_derivation = get_excess_derivation(mortality_daily)

    mortality_values = calculate_mortality_values(excess_derivation)
    assert set(mortality_values.keys()) >= set(['reference_date', 'annual_mortality_at_reference', 'model_annual_mortality_at_reference'])
    #pprint(mortality_values)
    #print(mortality_values['reference_date'].date())
    #print(type(mortality_values['reference_date']))



def test_visual_plot_excess_derivation_weekly_results(excess_derivation):
    excess_derivation_weekly = excess_derivation_to_weekly_resolution(excess_derivation)
    #fig, axes = plt.subplots(3, sharex=True, figsize=(12, 12))
    fig, axes = plt.subplots(2, 2, sharex=True, figsize=(18, 18))
    plot_excess_derivation_weekly_results(excess_derivation, excess_derivation_weekly, axes)



def test_visual_read_compute_plot(country, age, df_stat):
    excess_derivation, fig, axes, path = read_compute_plot(country, age, df_stat)


def test_calculate_mortality_values_for_ages_cumulative_ages_countries(df_stat, country):
    grid = itertools.product([country,], list(df_stat.columns.to_frame(False).AGE.unique()))
    covid_excess_fine_age, covid_excess_0_to_age, covid_excess_age_to_120 = calculate_mortality_values_for_ages_cumulative_ages_countries(df_stat, grid)


def test_align_df_stat_to_populationi(df_stat):
    df_stat = align_df_stat_to_population(df_stat)
    assert not '85-89' in df_stat.columns.to_frame(False).AGE.unique()
    assert '85-120' in df_stat.columns.to_frame(False).AGE.unique()

def test_sanitize_mortality_data():
    s = pd.Series([0, 0, 0, 0, 1, 2])
    expected = s.iloc[4:]
    assert np.allclose(sanitize_mortality_data(s).values, expected.values)

    s = pd.Series([1, 0, 0, 0, 1, 2])
    expected = s
    assert np.allclose(sanitize_mortality_data(s).values, expected.values)

