# from other sources:
est_israel_gdp_2019_billions_of_nis = 1400
est_ministry_of_finance_damage_billions_of_nis = {1 : 90, 2 : 130, 3 : 180, 4 : 250, '1D': 170}
corona_mortality_tot = {1 : 8600, 2 : 12700, 3 : 15700, 4 : 21600, '1B' : 4000, '2B' : 10000, '1D' : 12800}

# reasonable estimaes:
health_spending_weight_in_residual_life_expectancy = 0.5 #approx, the rest is technology

# other assumptions:
corona_normal_death_overlap_model = 'independent'

# scenario
scenario_number = '1D'
MOF_scenario = '1D'
MOH_scenario = '1D'
