import ast
import numpy as np
import pandas as pd


def extract_interval_end_from_text(l):
    return [int(s.split('-')[1]) for s in l]

def text_interval_index_to_interval_index(index, closed, sep='-', dtype=int):
    new_index = pd.IntervalIndex.from_arrays(*list(np.array([s.split(sep) for s in index], dtype=dtype).T), closed=closed)
    assert new_index.is_non_overlapping_monotonic
    return new_index

def closed_ranges_to_interval_index(l):
    return pd.IntervalIndex([pd.Interval(*ast.literal_eval(x), closed='both') for x in l])


def reduce_histogram_resolution(series, new_index, aggregation_type='sum'):
    cumsum_sabsampled = pd.Series(index=series.index.right, data=series.values.cumsum())[new_index.right]
    new_series = cumsum_sabsampled.diff()
    new_series.loc[new_series.index.values[0]] = cumsum_sabsampled.loc[new_series.index.values[0]]
    new_series.index = new_index
    return new_series


def locate_interval_in_interval_array(i, interval_array):
    left_label = interval_array.get_loc(i.left)
    right_label = interval_array.get_loc(i.right)
    if left_label != right_label:
        raise ValueError('other interval is not contained in any interval')
    return left_label

def rougher_binning(df, new_index, operation='sum'):
    assert df.index.is_non_overlapping_monotonic
    assert new_index.is_non_overlapping_monotonic
    labels = [locate_interval_in_interval_array(i, new_index) for i in df.index]
    groups = df.groupby(new_index[labels])
    if operation is 'sum':
        return groups.sum()
    elif operation is 'mean':
        return groups.mean()



def example_rougher_binning():
    index_1 = pd.IntervalIndex.from_tuples([(0, 4), (5, 9), (10, 14), (15,19)], closed='both')
    index_2 = pd.IntervalIndex.from_tuples([(0, 9), (10, 19)], closed='both')
    s = pd.Series(index=index_1, data=[1, 2, 3, 4])
    rougher_binning(s, index_2, 'mean')
    pd.concat([s, s], axis=1).groupby([index_2[0], index_2[0], index_2[1], index_2[1]]).sum()
