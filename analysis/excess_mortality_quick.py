from collections import OrderedDict
import itertools
from pathlib import Path
import numpy as np
import pandas as pd
import xarray as xr

import pytest
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib_params
from pprint import pprint

from PyPDF2 import PdfFileMerger
import datetime

from excess_mortality_funcs import parse_eurostat, parse_population, population_extrapolation_intrapolation, align_df_stat_to_population
from excess_mortality_funcs import read_compute_plot, plot_excess_derivation_weekly_results
from excess_mortality_funcs import parse_cdc_1



path_data = Path('../../mortality/eurostat/extract_1.csv')
path_population = Path('../../mortality/eurostat/population.csv')
path_results = Path('../../mortality/results/excess_mortality_quick')

IS_POPULATION = False
IS_USA = True

if IS_USA:
    path_data = Path('../../mortality/USA/Weekly_counts_of_deaths_by_jurisdiction_and_age_group.csv')

if __name__ == '__main__':
    if IS_USA:
        df_stat = parse_cdc_1(path_data)
    else:
        df_stat = parse_eurostat(path_data, is_calc_0_to_64=True)

    if IS_POPULATION:
        population = population_extrapolation_intrapolation(parse_population(path_population))
        df_stat = align_df_stat_to_population(df_stat)

    else:
        population = None


    #countries = ['Sweden', 'Italy', 'Spain']
    #countries = ['Sweden', 'Spain', 'Italy', 'Netherlands', 'Portugal', 'Denmark']
    countries = ['United States',]
    ages = df_stat.columns.to_frame(False).AGE.unique()
    paths = {}
    for country in countries:
        excess_derivation_many = OrderedDict()
        for age in ages:
            try:
                print(country, age)
                excess_derivation, _, _, path = read_compute_plot(country, age, df_stat, population=population, path_save=path_results)
                excess_derivation_many[age] = excess_derivation
                paths[(country, age)] = path
            except Exception as e:
                print(e)

        excess_derivation = sum(list(excess_derivation_many.values()))
        age_rep = 'Total of:' + ','.join(list(excess_derivation_many.keys()))
        adjustment = IS_POPULATION
        fig, axes = plt.subplots(2, 2, sharex=True, figsize=(18, 18))
        title = f'Population: {country}, Age: {age_rep}, Adjustment: {adjustment}'
        fig.suptitle(title)
        _, _, path = plot_excess_derivation_weekly_results(excess_derivation, axes=axes, path_save=path_results)
        paths[(country, age_rep)] = path


    pdfs = list(paths.values())
    pdfs = [str(pdf) for pdf in pdfs]
    print(pdfs)
    merger = PdfFileMerger()
    for pdf in pdfs:
        merger.append(pdf)
    merger.write(str(path_results.joinpath(f"excess_mortality_quick_{datetime.datetime.now().isoformat()}.pdf")))
    merger.close()

