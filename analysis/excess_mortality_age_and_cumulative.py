import itertools
from pathlib import Path
import numpy as np
import pandas as pd
import xarray as xr

import pytest
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib_params
from pprint import pprint

from excess_mortality_funcs import parse_eurostat, parse_population, population_extrapolation_intrapolation, align_df_stat_to_population
from excess_mortality_funcs import calculate_mortality_values_for_ages_cumulative_ages_countries, save_plot_covid_excess


path_data = Path('../../mortality/eurostat/extract_1.csv')
path_population = Path('../../mortality/eurostat/population.csv')
path_results = Path('../../mortality/results/excess_mortality_age_and_cumulative')

IS_POPULATION = True

if __name__ == '__main__':
    df_stat = parse_eurostat(path_data, is_calc_0_to_64=True)
    if IS_POPULATION:
        population = population_extrapolation_intrapolation(parse_population(path_population))
        df_stat = align_df_stat_to_population(df_stat)
        adjustment = 'population 2020'

    else:
        population = None
        adjustment = None

    coutries = list(df_stat.columns.to_frame(False).GEO.unique())
    ages = list(df_stat.columns.to_frame(False).AGE.unique())
    grid = itertools.product(coutries, ages)
    covid_excess_fine_age, covid_excess_0_to_age, covid_excess_age_to_120 = calculate_mortality_values_for_ages_cumulative_ages_countries(df_stat, grid)
    fig_fine, fig_to_age, fig_from_age = save_plot_covid_excess(covid_excess_fine_age, covid_excess_0_to_age, covid_excess_age_to_120, adjustment=adjustment, path_results=path_results)
