import matplotlib

matplotlib.rcParams['figure.figsize'] = [18, 6]
#matplotlib.rcParams['axes.grid'] = True
#matplotlib.rcParams['axes.grid.which'] = 'both'
matplotlib.rcParams['lines.linewidth'] = 2.5


matplotlib.rcParams['axes.labelsize'] = 16
matplotlib.rcParams['xtick.labelsize'] = 16
matplotlib.rcParams['ytick.labelsize'] = 16
matplotlib.rcParams['axes.titlesize'] = 16
matplotlib.rcParams['font.size'] = 14



